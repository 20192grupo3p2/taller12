#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char **argv){
  umask(0);
  int ffuente, fdestino, count=0;
  ffuente = open(argv[1], O_RDWR | O_CREAT, 0666);
  fdestino = open(argv[2], O_WRONLY | O_TRUNC | O_CREAT, 0666);

  if (ffuente < 0){
		perror("No se pudo abrir o crear el archivo.");
    exit(1);
  }

  while(read(ffuente, &argv[1], 1) != 0){
    if (write(fdestino, &argv[1], 1) != 1) {
      exit(20);
    }
    count += 1;
  }

	printf("Bytes escritos: %d\n", count);
  close(ffuente);
  close(fdestino);
  return 0;

}
